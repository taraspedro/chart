-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 01 2016 г., 00:03
-- Версия сервера: 5.5.45
-- Версия PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `orgchart`
--

-- --------------------------------------------------------

--
-- Структура таблицы `chart`
--

CREATE TABLE IF NOT EXISTS `chart` (
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL,
  `floor` int(11) NOT NULL,
  `earnings` int(11) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `chart`
--

INSERT INTO `chart` (`company_id`, `name`, `parent`, `floor`, `earnings`) VALUES
(1, 'Apple Inc.', 0, 0, 80),
(2, 'Flyby Media', 1, 1, 70),
(3, 'LearnSprout', 1, 1, 70),
(4, 'Emotient', 1, 1, 70),
(5, 'LegbaCore', 2, 2, 60),
(6, 'Faceshift', 2, 2, 60),
(7, 'Perceptio', 2, 2, 60),
(8, 'VocalIQ', 3, 2, 60),
(9, 'Mapsense', 3, 2, 60),
(10, 'Metaio', 4, 2, 60),
(11, 'Coherent Navigation', 7, 3, 40),
(12, 'LinX', 7, 3, 40),
(13, 'FoundationDB', 8, 3, 40),
(14, 'Semetric', 8, 3, 40),
(15, 'Camel Audio', 10, 3, 40),
(16, 'Dryft', 10, 3, 40),
(17, 'Beats Electronics', 10, 3, 40);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
