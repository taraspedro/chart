# README #

This web App is for easy designing company chart. Here you can view, edit or remove information about general company or subsidiaries.

###Used technologies ###

* Back-end: PHP, MySQL

* Front-end: HTML, CSS, JS(jQuery), JSON, AJAX

### How to install? ###

* Download this repository into your machine and put it to your localhost

* Install my database (orgchart.sql)

* Edit configuration file (/bin/db.php)

* Enjoy App

### How it works? ###

* All information is stored into SQL database. With the help of PHP I encode the information to JSON. Then, with the help of AJAX and some itterations, I create the DOM tree. 

* This App can view company chart, edit data, add or remove companies from chart. 

* If you want to customize company data, you have to click on company name in the tree and click on "Customize company data:" link in the right panel below the tree.