$(function() {
	//define some global variables
	var name, earnings, parentId, selfId, parentName,  newId;

	// define function to create DOM
	function createChart(){
			$.ajax({
			url: 'bin/datachart.php',
			type: 'GET',
			dataType: 'JSON',
			success: function(json){
				var j = 0;
				var out = '';
				var arr = json;
				// console.log(arr[2]['company_id']);
				
				function getMax(arr, prop) {
				    var max;
				    for (var i=0 ; i<arr.length ; i++) {
				        if (!max || parseInt(arr[i][prop]) > parseInt(max[prop]))
				            max = arr[i];
				    }
				    return max;
				}

				var maxParent = getMax(arr, "parent");
				// console.log(maxParent.parent);
				
				var arr1 = jQuery.grep(arr, function(obj) {
				    return obj.parent === "0";
				});
				while(j<=arr1.length-1){
					$('.tree').append('<ul></ul>') 
					out = "<li class='total'><a id =" + json[j].company_id + " data-parent=" + json[j].parent +" data-earnings=" + json[j].earnings + ">" + json[j].name + "</a></li>"; 
					j++;
				};			
				$('.tree ul').append(out);	

				// magic is here
				for (var z=1; z<=maxParent.parent; z++){
					var arr2 = jQuery.grep(arr, function(obj) {
					    return obj.parent == z;
					});
					if(arr2.length == 0){
						continue;
					}				
					// console.log(z, arr2, arr2.length);
					$( "#" + z ).after('<ul></ul>');	
					for (var k=0; k<arr2.length; k++){
						$('#' + arr2[k]["parent"]).next().append('<li><a id=' + arr2[k]["company_id"] +' data-earnings=' + arr2[k]["earnings"] + ' data-parent=' + arr2[k]["parent"] +'>' + arr2[k]["name"] + '</a></li>');
					}
				}

				// correction
				var b;
				for(b=1;b<arr.length;b++){
					if($('a#' + arr[b]['company_id']).length === 0){			
						// console.log(b, arr[b]['parent'], arr[b]['company_id'], arr[b]['name']);
						console.log($("a#" + arr[b]['parent']).next().prop('tagName'));
						if($("a#" + arr[b]['parent']).next().prop('tagName') == undefined){
							$("a#" + arr[b]['parent']).after('<ul></ul>');
						}								
						$('a#' + arr[b]['parent']).next().append('<li><a id=' + arr[b]["company_id"] +' data-earnings=' + arr[b]["earnings"] + ' data-parent=' + arr[b]["parent"] +'>' + arr[b]["name"] + '</a></li>');							
					}
				}
				
				

				// define behaviour on tree item click
				$('.tree').on('click', 'a', function(e){
					e.preventDefault();
					earnings = $(this).data('earnings');
					name  = $(this).text();
					// parentId = $(this).data('parent');
					selfId = parseInt($(this).attr('id'));
					// console.log(selfId,parentId);
					var childs = $(this).next().find('a');
					var childsLength = childs.length;
					var totalEarnings = earnings;
					var parent = $(this).parent().parent().prev().text();
						for (var i = 0; i<childsLength; i++){
							totalEarnings = totalEarnings + childs.eq(i).data('earnings');
						}
					// console.log(earnings, childsLength, childs, totalEarnings);
					$('input#company-name').val(name);
					$('input#company-parent').val(function() {
						if(parent == ''){
							return 'General company';
						}
						else{
							return parent;
						}
					});
					// console.log($(this).parent().parent().prev().text());
					$('input#earnings').val(earnings);
					$('input#totat').val(function() {
						if($('#' + selfId).parent().find('ul').length == 0){
							return 'Has no subsidiaries';
						}
						else{
							return totalEarnings;
						}
					});
				});	

				// General panel settings
				$('.well[data-general="name"] .result').html($('.tree a:first').text());
				$('.well[data-general="number"] .result').html($('.tree a').length);
				var IncEarnings = 0;
				for(var i = 0; i<$('.tree a').length; i++){
					IncEarnings = IncEarnings + $('.tree a').eq(i).data('earnings');
				}
				$('.well[data-general="earnings"] .result').html(IncEarnings);
				var newWidth = $('.tree').width() + 200;
				$('body').css('min-width',newWidth);
			}			
		});
	}

	// launch DOM creation
	createChart();

	// disable inputs while reload the page
	$('input#company-name, input#company-parent, input#earnings').prop('disabled', true);
		
	// data customization settings
	$('a.custom-link').on('click', function(e){
		e.preventDefault();
		$('.btn-group-justified').slideToggle('fast');
		$('input#company-name, input#company-parent, input#earnings').prop('disabled', function(i, v) { return !v;});
	});

	// getting form values
	$('.btn-group-justified a').on('click',function(){
		name = $('input#company-name').val();
		// selfId = $("a:contains(" + name + ")").attr('id');
		parentName = $('input#company-parent').val();
		parentId = parseInt($("a:contains(" + parentName + ")").attr('id'));
		earnings = $('input#earnings').val();
		newId  = $('.tree a').length +1;
		setTimeout(function() {
			$('.form-horizontal').find('.alert').fadeOut();
		}, 4000);
	});	

	//define callback after tree altering
	function alterCall(){
		setTimeout(function() {
			$('.tree').empty();	
			createChart();
		}, 2000);
	}

	// update company info
	$('a.btn-info').on('click', function(e){
		e.preventDefault();
		console.log(selfId,name,parentId,earnings);
		if($("a:contains(" + parentName + ")").length == 0 && parentName != 'General company'){
			$(this).parent().parent().append('<div class="alert alert-danger" role="alert">Parent name is incorrect</div>');			
		}
		else{
			// console.log(name,selfId,parentName,parentId,earnings);	
			$.post('bin/update.php',{selfId:selfId,name:name,parentId:parentId,earnings:earnings},function(data){
				$('.btn-group-justified').parent().append(data);
			});
			alterCall();			
		}
	});

	// add new company
	$('a.btn-success').on('click',function(e){
		e.preventDefault();
		console.log(parentName,name,parentId,earnings);
		if(parentName == 'General company'){
			$(this).parent().parent().append('<div class="alert alert-danger" role="alert">Only one general company allowed</div>');
		}
		else if($("a:contains(" + parentName + ")").length == 0){
			$(this).parent().parent().append('<div class="alert alert-danger" role="alert">Parent name is incorrect</div>');			
		}
		else if($("a:contains(" + name + ")").length > 0){
			$(this).parent().parent().append('<div class="alert alert-danger" role="alert">Company with  this name is already exist</div>');
		}		
		else{
			console.log(name,selfId,parentName,parentId,earnings);
			$.post('bin/insert.php',{newId:newId,name:name,parentId:parentId,earnings:earnings},function(data){
				$('.btn-group-justified').parent().append(data);
				alterCall();
			});			
		}
	});

	// remove company
	$('a.btn-danger').on('click',function(e){
		e.preventDefault();
		// console.log($('#' + selfId).parent().find('ul').length);
		if($('#' + selfId).parent().find('ul').length > 0){
			$(this).parent().parent().append('<div class="alert alert-danger" role="alert">You can not remove company with subsidiaries</div>');			
		}
		else{
			$.post('bin/remove.php',{selfId:selfId,name:name,parentId:parentId,earnings:earnings},function(data){
				$('.btn-group-justified').parent().append(data);
				alterCall();
			});
		}
	});
});